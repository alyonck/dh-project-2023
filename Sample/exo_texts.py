import json

data = json.load(open('echo.json', 'r', encoding='utf-8'))
data1 = data['messages']
array = []

for item in data1:
    piece = item['text']
    if type(piece) == str:
        if 'выпуск новостей' in piece:
            piece = piece.split('\n\n')
            for i in range(1, len(piece)):
                array.append(piece[i])
        else:
            array.append(piece)
    else:
        string = ''
        for i in range(len(piece)):
            if type(piece[i]) == dict:
                string += piece[i]['text']
            elif type(piece[i]) == str:
                string += piece[i]
        array.append(string)


with open('full_echo_texts.txt', 'w', encoding='utf-8') as o:
    o.write(str(array))
