# этот код разбирает тексты сатирических каналов
import json


def irony(array, data):
    for item in data:  # в этих каналах нет никаких сводок, поэтому сообщения не нужно никак разбивать
        piece = item['text']
        if type(piece) == str:
            array.append(piece)
        else:
            string = ''
            for i in range(len(piece)):
                if type(piece[i]) == dict:
                    string += piece[i]['text']
                elif type(piece[i]) == str:
                    string += piece[i]
            array.append(string)
    return array


data_pezduza = json.load(open('data/pezduza.json', 'r', encoding='utf-8'))  # разбор канала Пездуза
data_pzdz = data_pezduza['messages']
array_pezuza = []
array_pezuza = irony(array_pezuza, data_pzdz)
with open('data/pezduza_texts.txt', 'w', encoding='utf-8') as o:
    o.write(str(array_pezuza))

data_neuromeduza = json.load(open('data/neuromeduza.json', 'r', encoding='utf-8'))  # разбор канала Нейромедуза
data_nrmdz = data_neuromeduza['messages']
array_neuromeduza = []
array_neuromeduza = irony(array_neuromeduza, data_nrmdz)
with open('data/neuromeduza_texts.txt', 'w', encoding='utf-8') as ou:
    ou.write(str(array_neuromeduza))




