# этот код разбирает тексты новостных телеграм каналов. для каждого канала создается два текстовых файла: один содержит
# массив с текстами новостей, второй содержит словарь, где каждой новости сопоставлена дата создания новости. они будут
# лежать в директории data и называются "название канала"_texts.txt и "название канала"_dict.json соответственно
# для успешной работы кода нужно скачать архивы в формате json, они лежат в папке data

import json


# эта функция добавляет текст, который хранится одним входом, в массив и словарь
def strings(some_piece, some_date, some_dict, some_texts):
    if some_piece != "":
        some_texts.append(some_piece)
        some_dict[some_piece] = some_date
    return


# эта функция собирает текст внутри одного сообщения, который из-за форматирования разбивается на элементы, которые
# хранятся в словаре,в одну строку
def to_string(some_piece, st):
    for j in range(len(some_piece)):
        if type(some_piece[j]) == dict:
            st += some_piece[j]['text']
        elif type(some_piece[j]) == str:
            st += some_piece[j]
    return st


# эта функция разбирает простые случаи, где есть только две "рубрики" - обычные новости и сводки за день. если сводку
# за день можно распознать по одному хэштегу или кодовому слову, то используется эта функция. marker = хэштег или
# кодовое слово, separator - как разделяются новости внутри сообщения. в других случаях функция немного модифицируется.
def basic_cases(texts, dicti, some_data, marker, separator):
    for it in some_data:
        some_piece = it['text']
        some_date = it['date']
        if type(some_piece) == str:
            strings(some_piece, some_date, dicti, texts)
        else:
            string = ''
            string = to_string(some_piece, string)
            if marker in string:
                string = string.split(separator)
                for k in range(1, len(string)):  # в первом элементе остается только заголовок, поэтому он нам не нужен
                    texts.append(string[k])
                    dicti[string[k]] = some_date
            else:
                texts.append(string)
                dicti[string] = some_date
    return dicti, texts


# разбор канала "Редакция новости"
data_red = json.load(open('data/redakcia.json', 'r', encoding='utf-8'))
data1_red = data_red['messages']
redakcia_texts = []
redakcia_dict = {}
for item in data1_red:
    piece = item['text']
    date = item['date']
    if type(piece) != str:  # из-за форматирования сообщения (добавление шрифтов, ссылок и тд) большинство новостей идут
        # в формате словаря, внутри которого текст разбит на кусочки
        string_red = ''
        string_red = to_string(piece, string_red)
        if "#ньюсдня" in string_red:  # в разделе ньюсдня собраны новости за весь день, поэтому в одном сообщение могут
            # быть не связанные между собой события. нужно разбить это на разные сообщения
            string_red = string_red.split('\n\n—')
            for elem in string_red:
                if "#ньюсдня" not in elem:  # в первом элеиенте разобранного сообщения только дата и плашка об
                    # иноагентстве :), можно не добавлять это в наши данные
                    redakcia_texts.append(elem)
                    redakcia_dict[elem] = date
        elif "Фото дня" or "Видео и фото дня" in string_red:  # в этих сообщениях нет текста или он повторяет ньюсдня
            pass
        else:
            redakcia_texts.append(string_red)
            redakcia_dict[string_red] = date
    else:
         strings(piece, date, redakcia_dict, redakcia_texts)
with open('data/redakcia_texts.txt', 'w', encoding='utf-8') as redakcia_t:
    redakcia_t.write(str(redakcia_texts))
with open('data/redakcia_dict.json', 'w', encoding='utf-8') as redakcia_d:
    json.dump(redakcia_dict, redakcia_d, indent=True, ensure_ascii=False)


# разбор канала "SVTV NEWS"
data_svtv = json.load(open('data/svtvnews.json', 'r', encoding='utf-8'))
data1_svtv = data_svtv['messages']
svtv_texts = []
svtv_dict = {}
for item in data1_svtv:
    piece = item['text']
    date = item['date']
    if type(piece) == str:  # рассматриваем сообщения, тип которых str, убираем пустые строки
        strings(piece, date, svtv_dict, svtv_texts)
    else:
        string_svtv = ''
        string_svtv = to_string(piece, string_svtv)
        if "Что случилось" and "самое важное" in string_svtv:  # в разделе что случилось самое важное собраны новости за
            # весь день, поэтому в одном сообщение могу быть не связанные между собой события. нужно разбить это
            string_svtv = string_svtv.split('\n\n-')
            for i in range(1, len(string_svtv)):
                svtv_texts.append(string_svtv[i])
                svtv_dict[string_svtv[i]] = date
        else:
            svtv_texts.append(string_svtv)
            svtv_dict[string_svtv] = date

with open('data/svtvnews_texts.txt', 'w', encoding='utf-8') as svtvnews_t:
    svtvnews_t.write(str(svtv_texts))
with open('data/svtvnews_dict.json', 'w', encoding='utf-8') as svtvnews_d:
    json.dump(svtv_dict, svtvnews_d, indent=True, ensure_ascii=False)

# разбор канала Дождь
data_rain = json.load(open('data/tvrain.json', 'r', encoding='utf-8'))
data1_rain = data_rain['messages']
rain_texts = []
rain_dict = {}
for item in data1_rain:
    piece = item['text']
    date = item['date']
    if type(piece) == str:
        strings(piece, date, rain_dict, rain_texts)
    else:
        string_rain = ''
        string_rain = to_string(piece, string_rain)
        if "Программа «Здесь и сейчас»" or "Включайте Дождь" or "Включайте новости" or "В студии" in string_rain:
            # в таких сообщениях анонсы эфиров, которые посвящены разным новстям, через \n\n перечисляются разные
            # новости, поэтому нужно разбить эти сообщения на разные
            string_rain = string_rain.split('\n\n')
            for i in range(2, len(string_rain)):
                rain_texts.append(string_rain[i])
                rain_dict[string_rain[i]] = date
        else:
            rain_texts.append(string_rain)
            rain_dict[string_rain] = date
with open('data/tvrain_texts.txt', 'w', encoding='utf-8') as tvrain_t:
    tvrain_t.write(str(rain_texts))
with open('data/tvrain_dict.json', 'w', encoding='utf-8') as tvrain_d:
    json.dump(rain_dict, tvrain_d, indent=True, ensure_ascii=False)


# разбор канала ТАСС
data_tass = json.load(open('data/tass.json', 'r', encoding='utf-8'))
data1_tass = data_tass['messages']
tass_texts = []
tass_dict = {}
tass_d, tass_t = basic_cases(tass_texts, tass_dict, data1_tass, "#Главные_события_ТАСС", '\n\n')
with open('data/tass_texts.txt', 'w', encoding='utf-8') as t_tass:
     t_tass.write(str(tass_t))
with open('data/tass_dict.json', 'w', encoding='utf-8') as d_tass:
    json.dump(tass_d, d_tass, indent=True, ensure_ascii=False)

# разбор канала RT
data_rt = json.load(open('data/rt.json', 'r', encoding='utf-8'))
data1_rt = data_rt['messages']
rt_texts = []
rt_dict = {}
rt_d, rt_t = basic_cases(rt_texts, rt_dict, data1_rt, "#Главное_на_RT", '\n\n-')
with open('data/rt_texts.txt', 'w', encoding='utf-8') as t_rt:
     t_rt.write(str(rt_t))
with open('data/rt_dict.json', 'w', encoding='utf-8') as d_rt:
    json.dump(rt_d, d_rt, indent=True, ensure_ascii=False)

# разбор канала РИА НОВОСТИ
data_ria = json.load(open('data/ria.json', 'r', encoding='utf-8'))
data1_ria = data_ria['messages']
ria_texts = []
ria_dict = {}
for item in data1_ria:
    piece = item['text']
    date = item['date']
    if type(piece) == str:
        if "Спецоперация, " and " Главное:" in piece:  # в этом канале сводки новостей могут быть как полным сообщением,
            # так и разбитыми на куски из-за форматирования
            piece = piece.split(':\n\n')
            if len(piece) > 1:
                piece_ria = piece[1]
                piece_ria = piece_ria.split('\n')
                for elem in piece_ria:  # в первом элементе остается только заголовок, поэтому он нам не нужен
                    ria_texts.append(elem)
                    ria_dict[elem] = date
            else:  # форматирование новости не всегда одинаковое
                piece = piece[0].split('\n')
                for elem in piece:
                    ria_texts.append(elem)
                    ria_dict[elem] = date
        else:
            ria_texts.append(piece)
            ria_dict[piece] = date

    else:
        string_ria = ''
        string_ria = to_string(piece, string_ria)
        if "Спецоперация, " and " Главное:" in string_ria:  # аналогично варианту, где тип str
            string_ria = string_ria.split(':\n\n')
            if len(string_ria) > 1:
                str_ria = string_ria[1]
                str_ria = str_ria.split('\n')
                for elem in str_ria:
                    ria_texts.append(elem)
                    ria_dict[elem] = date
            else:
                string_ria = string_ria[0].split('\n')
                for elem in string_ria:
                    ria_texts.append(elem)
                    ria_dict[elem] = date

        else:
            ria_texts.append(string_ria)
            ria_dict[string_ria] = date
with open('data/ria_texts.txt', 'w', encoding='utf-8') as t_ria:
    t_ria.write(str(ria_texts))
with open('data/ria_dict.json', 'w', encoding='utf-8') as d_ria:
    json.dump(ria_dict, d_ria, indent=True, ensure_ascii=False)


# разбор канала 1news
data_1tv = json.load(open('data/1tv.json', 'r', encoding='utf-8'))
data1_1tv = data_1tv['messages']
tv1_texts = []
tv1_dict = {}
for item in data1_1tv:
    piece = item['text']
    date = item['date']
    if type(piece) == str:
        strings(piece, date, tv1_dict, tv1_texts)
    else:
        string_1tv = ''  # в этом канале нет сводок новостей, поэтому  не надо ничего разделять
        string_1tv = to_string(piece, string_1tv)
        tv1_texts.append(string_1tv)
        tv1_dict[string_1tv] = date
with open('data/1tv_texts.txt', 'w', encoding='utf-8') as t_1tv:
    t_1tv.write(str(tv1_texts))
with open('data/1tv_dict.json', 'w', encoding='utf-8') as d_1tv:
    json.dump(tv1_dict, d_1tv, indent=True, ensure_ascii=False)


# разбор канала tayga news
data_tayga = json.load(open('data/tayga.json', 'r', encoding='utf-8'))
data1_tayga = data_tayga['messages']
tayga_texts = []
tayga_dict = {}
tayga_dict, tayga_texts = basic_cases(tayga_texts, tayga_dict, data1_tayga, 'Всё самое главное о последствиях событий',
                                      '\n\n❗')
with open('data/tayga_texts.txt', 'w', encoding='utf-8') as t_tayga:
    t_tayga.write(str(tayga_texts))
with open('data/tayga_dict.json', 'w', encoding='utf-8') as d_tayga:
    json.dump(tayga_dict, d_tayga, indent=True, ensure_ascii=False)


# разбор канала it's my city
data_itsmycity = json.load(open('data/itsmycity.json', 'r', encoding='utf-8'))
data1_itsmycity = data_itsmycity['messages']
itsmycity_texts = []
itsmycity_dict = {}
for item in data1_itsmycity:
    piece = item['text']
    date = item['date']
    if type(piece) == str:
        strings(piece, date, itsmycity_dict, itsmycity_texts)
    else:
        string_itsmycity = ''  # в этом канале нет сводок новостей, поэтому  не надо ничего разделять
        string_itsmycity = to_string(piece, string_itsmycity)
        itsmycity_texts.append(string_itsmycity)
        itsmycity_dict[string_itsmycity] = date
with open('data/itsmycity_texts.txt', 'w', encoding='utf-8') as t_itsmycity:
    t_itsmycity.write(str(itsmycity_texts))
with open('data/itsmycity_dict.json', 'w', encoding='utf-8') as d_itsmycity:
    json.dump(itsmycity_dict, d_itsmycity, indent=True, ensure_ascii=False)
