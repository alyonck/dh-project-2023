# Этот код строит графики упоминаемости отдельных людей

import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime


def merge(dts1, names1, dts2, names2):  # эта функция совмещает данные из двух каналов
    peopl = []
    dts = []
    for p in names1:
        index1 = names1.index(p)
        if p in names2:
            index2 = names2.index(p)
            d = dts1[index1] + dts2[index2]
            peopl.append(p)
            dts.append(d)
        else:
            peopl.append(p)
            dts.append(dts1[index1])
    for p in names2:
        index2 = names2.index(p)
        if p not in peopl:
            peopl.append(p)
            dts.append(dts2[index2])
    return peopl, dts


print("Эта программа повзоляет вам построить график употреблений человека в период с 28 сентября 2022 года по 5 апреля "
      "2023 года. Выберите каналы, по данным которых вы хотите построить график. Для этого напишите сокращенное "
      "название канала или напишите сокращенные названия всех интересующих вас каналов разделяя пробелами.\n Список "
      "каналов: Пивоваров (редакция): RED\nТелеканал Дождь: RAIN\nТАСС: TASS\nПервый канал. Новости: TV1\nРИА Новости: "
      "RIA\nТайга.инфо: TAIGA\nIt's My City: MYCITY\nRT на русском: RT")
titles = {'RED': [], 'RAIN': [], 'TASS': [], 'TV1': [], 'RIA': [], 'TAIGA': [], 'MYCITY': [], 'RT': []}
inp = str(input())
inp = inp.split(' ')
if len(inp) == 1:  # если пользователь хочет посмотреть только на один канал
    inp = str(inp[0])
    with open(f'TXTs/{inp.lower()}_txt.txt', 'r', encoding='utf-8') as f:
        dates = f.read()
        dates = eval(dates)
    with open(f'matrix and keys/{inp.lower()}.txt', 'r', encoding='utf-8') as n:
        people = n.read()
        people = eval(people)
        print(people)
else:  # если пользователь хочет посмотреть на несколько каналов
    dates = []
    people = []
    for elem in inp:
        if elem in titles.keys():
            with open(f'matrix and keys/{elem}.txt', 'r', encoding='utf-8') as n:
                people_ = n.read()
                people_ = eval(people_)
            with open(f'TXTs/{elem.lower()}_txt.txt', 'r', encoding='utf-8') as f:
                dates_ = f.read()
                dates_ = eval(dates_)
            people, dates = merge(dates, people, dates_, people_)
        else:
            print('Пожалуйста, введите названия в нужном формате')
            break

for per in people:
    print(per)
pers = input('Выберите человека, график упоминаемости которого вы хотите посмотреть\n')
index = people.index(pers)
date_ = dates[index]

dates_array = []
for elem in dates:  # делаем массив со всеми датами, когда употребляются люди в этом канале
    if type(elem) == list:
        for el in elem:
            if el not in dates_array:
                dates_array.append(str(el))
    else:
        if elem not in dates_array:
            dates_array.append(str(elem))
dates_array = sorted(dates_array)

# нужно поделить даты на группы по 10 дней, чтобы график был не очень захламленным
dates_array = [datetime.strptime(str(date), '%Y-%m-%d') for date in dates_array]
dates_array = sorted(dates_array)
min_date = dates_array[0]
max_date = dates_array[-1]
num_groups = int((max_date - min_date).days / 10) + 1
date_groups = [[] for _ in range(num_groups)]
for date in dates_array:
    group_index = int((date - min_date).days / 10)
    date_groups[group_index].append(date)
date_groups = [[datetime.strftime(date, '%Y-%m-%d') for date in group] for group in date_groups]

# теперь нужно присвоенным группам дать названия, удобнее всего это сделать в виде словаря
groups = dict()
for i in range(len(date_groups)):
    name1 = date_groups[i][0].replace('-', '.')
    name2 = date_groups[i][-1].replace('-', '.')
    name = str(name1) + '-' + str(name2)
    groups.update({f'{name}': date_groups[i]})

# для строим график для человека
mentions = date_
person = pers
ment = dict()
results = dict()
for dat in mentions:  # смотрим, сколько раз человек употребляется каждый день
    if dat in ment.keys():
        ment[dat] += 1
    else:
        ment.update({dat: 1})
for key in ment.keys():  # сопоставляем даты определенным ранее периодам
    for item in groups.items():
        name, group = item
        if key in group:
            if name not in results.keys():
                results.update({name: ment[key]})
            else:
                results[name] += 1
dataframe = pd.DataFrame({'date': sorted(results.keys()),  # строим график
                          'person': sorted(results.values())})

plt.plot_date(dataframe.date, dataframe.person)
plt.xticks(rotation=30, ha='right')
plt.title(f'{person}')
plt.xlabel('Временной отрезок')
plt.ylabel('Количество упоминаний')
plt.show()
