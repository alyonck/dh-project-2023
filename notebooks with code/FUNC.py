# подключаем библиотеку для работы с именами собственными
from natasha import (
    Segmenter,
    MorphVocab,
    
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,
    
    PER,
    NamesExtractor,

    Doc
)

segmenter = Segmenter()
morph_vocab = MorphVocab()

emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)
syntax_parser = NewsSyntaxParser(emb)
ner_tagger = NewsNERTagger(emb)

names_extractor = NamesExtractor(morph_vocab)


# выгружаем библиотеку из json-файла
import json
Dict = json.load(open('путь к файлу'))

# проходясь по файлу, создаем библиотеку, которая хранит имена собственные по типам и для каждого имени хранит даты публикации
prp_in_news = {'NAMES': dict(),
              'LOCS': dict(),
              'ORGS': dict()}
for news in Dict:
    doc = Doc(news)
    doc.segment(segmenter)
    doc.tag_morph(morph_tagger)
    for token in doc.tokens:
        token.lemmatize(morph_vocab)
    doc.tag_ner(ner_tagger)
    for span in doc.spans:
        span.normalize(morph_vocab)
        if span.type == 'PER':
            if Dict[news] not in prp_in_news['NAMES']:     
                prp_in_news['NAMES'][Dict[news]] = set()
            prp_in_news['NAMES'][Dict[news]].add(span.normal)
        elif span.type == 'ORG':
            if Dict[news] not in prp_in_news['ORGS']:     
                prp_in_news['ORGS'][Dict[news]] = set()
            prp_in_news['ORGS'][Dict[news]].add(span.normal)
        elif span.type == 'LOC':
            if Dict[news] not in prp_in_news['LOCS']:    
                prp_in_news['LOCS'][Dict[news]] = set()
            prp_in_news['LOCS'][Dict[news]].add(span.normal)
            
# теперь создаем библиотеку, где каждому имени-ключу будут соотвествовать даты публикаций новостей с их участием
# Сначала проходимся по полным именам, а потом уже проходимся еще раз и добавляем неполные имена к полным
keys_for_matrix = dict()
for i in prp_in_news['NAMES']:
    for j in prp_in_news['NAMES'][i]:
        if j.count(' ') >= 1:
            if j in keys_for_matrix:
                keys_for_matrix[j][0] += 1
                keys_for_matrix[j][1].add(i)
            else:
                keys_for_matrix[j] = [1, set()]
                keys_for_matrix[j][1].add(i)
                
for i in prp_in_news['NAMES']:
    for j in prp_in_news['NAMES'][i]:
        if j.count(' ') == 0:
            for name in keys_for_matrix:
                if j in name:
                    print(j)
                    keys_for_matrix[name][0] += 1
                    keys_for_matrix[name][1].add(i)
                    break

# на этом этапе можно вручну подкорректировать получающуюся библиотеку:
# если один человек встречается дважды, то его можно можно свести к одному упоминанию:
# keys_for_matrix['Илон Маск'][0] += keys_for_matrix['Илона Маска'][0] -- суммируем количество упоминаний
# keys_for_matrix['Илон Маск'][1] = keys_for_matrix['Илон Маск'][1].union(keys_for_matrix['Илона Маска'][1]) -- объединяем множества дат публикаций
# keys_for_matrix.pop('Илона Маска') -- удаляем ненужное имя
# если же в библиотеке попалось то, что именем не является, то этот элемент нужно удалить:
# keys_for_matrix.pop('Минцифры')

# теперь нужно составить матрицы с количеством и датами совместных упоминаний: просто проходимся по библиотеке и смотрим на пересечение множеств и их длину
M_TASS_numb = []
M_TASS_dates = []
for G in keys_for_matrix:
    M_TASS_numb.append(list())
    M_TASS_dates.append(list())
    for V in keys_for_matrix:
        count = 0
        l = []
        if G != V:
            for m in keys_for_matrix[G][1]:
                for n in keys_for_matrix[V][1]:
                    if m == n:
                        count += 1
                        l.append(n)
        M_TASS_numb[-1].append(count)
        M_TASS_dates[-1].append(l)
