import math
import pandas as pd
import networkx as nx
import plotly.graph_objects as go


def make_edge(x, y, text, width):
    return go.Scatter(x=x,
                      y=y,
                      line=dict(width=width,
                                color='#0057fa'),
                      hoverinfo='text',
                      text=([text]),
                      mode='lines')


class Connection:  # возможно мне не нужен этот класс? ну я на всякий случай храню много всего
    def __init__(self, dot1_: int, dot2_: int, weight_):  # передаём номера (как они стоят в persons)
        global persons
            if weight_ is None:
        weight_ = 0
        self.dot1 = persons[dot1_]
        self.dot2 = persons[dot2_]
        self.dot1_num = dot1_
        self.dot2_num = dot2_
        self.weight = int(weight_)
        if self.weight == 0:
            self.log_weight = 0
        else:
            self.log_weight = math.log(self.weight, 6)
        self.normalized_weight = self.weight / 15

    def print(self):
        print(self.dot1, self.dot2, self.weight)


with open("start_message.txt", encoding="utf8") as f:
    print(f.read())
chan_name = input().strip().upper()

print("Каким будет минимальное количество общих упоминаний, чтобы связь попала на график?\n"
      "Если оно маленькое, то вы увидите более полную картину, а если большое, то будет проще понять,"
      "что происходит на графе.\nДля разных каналов подойдут разные.\nВведите целое число")
threshold = int(input())

input_csv = f"../matrix and keys/{chan_name}_numb.csv"
input_columns = f"../matrix and keys/{chan_name}.txt"

with open(input_columns, encoding="utf8") as f:
    persons_line = f.readline()
persons_line = persons_line[1:-1]
persons_list = persons_line.split(",")
persons = []
for i in persons_list:
    persons.append(i.strip()[1:-1])

matrix = pd.read_csv(input_csv, names=persons)  # у нас строки и столбцы вообще-то одинаковые вещи означают,
# но столбцы называются по персонажам, а строки по номерам


connections = []
non_empty_dots = []
min_weight = threshold
for i in range(len(persons)):
    degree = 0
    for j in range(i, len(persons)):
        new_edge = Connection(i, j, matrix[persons[i]][j])
        connections.append(new_edge)
        if new_edge.weight >= min_weight:
            degree += 1
    if degree != 0:
        non_empty_dots.append(persons[i])

edges = []
for i in connections:
    if i.weight >= min_weight:
        edges.append((i.dot1, i.dot2, i.normalized_weight))

G = nx.Graph()
G.add_nodes_from(non_empty_dots)
G.add_weighted_edges_from(edges)
pos = nx.spring_layout(G)  # точки раскладываются алгоритмом Фрюхтермана-Рейнгольтса

edge_trace = []
for edge in G.edges():
    if G.edges()[edge]['weight'] > 0:
        char_1 = edge[0]
        char_2 = edge[1]
        x0, y0 = pos[char_1]
        x1, y1 = pos[char_2]
        text = char_1 + '--' + char_2 + ': ' + str(G.edges()[edge]['weight'])
        trace = make_edge([x0, x1, None], [y0, y1, None], text,
                          width=2 * G.edges()[edge]['weight'] ** 1.5)
        edge_trace.append(trace)

node_trace = go.Scatter(x=[],
                        y=[],
                        text=[],
                        textposition="top center",
                        mode='markers+text',
                        hoverinfo='none',
                        marker=dict(color=[],
                                         size=[],
                                         line=None))

for node in G.nodes():
    x, y = pos[node]
    node_trace['x'] += tuple([x])
    node_trace['y'] += tuple([y])
    node_trace['marker']['color'] += tuple(['cornflowerblue'])
    node_trace['text'] += tuple(['<b>' + node + '</b>'])

layout = go.Layout(
    paper_bgcolor='rgba(0,0,0,0)', # transparent background
    plot_bgcolor='rgba(0,0,0,0)', # transparent 2nd background
    xaxis={'showgrid': False, 'zeroline': False},  # no gridlines
    yaxis={'showgrid': False, 'zeroline': False},  # no gridlines
    title=chan_name
)
# Create figure
fig = go.Figure(layout = layout)
# Add all edge traces
for trace in edge_trace:
    fig.add_trace(trace)
# Add node trace
fig.add_trace(node_trace)
# Remove legend
fig.update_layout(showlegend = False)
# Remove tick labels
fig.update_xaxes(showticklabels = False)
fig.update_yaxes(showticklabels = False)
# Show figure
fig.show()
